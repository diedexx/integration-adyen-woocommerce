const path = require( 'path' );
const defaultConfig = require( '@wordpress/scripts/config/webpack.config' );
const WooCommerceDependencyExtractionWebpackPlugin = require( '@woocommerce/dependency-extraction-webpack-plugin' );
const MiniCssExtractPlugin = require( 'mini-css-extract-plugin' );

// Remove SASS rule from the default config so we can define our own.
const defaultRules = defaultConfig.module.rules.filter( ( rule ) => {
   return String( rule.test ) !== String( /\.(sc|sa)ss$/ );
} );

module.exports = {
   ...defaultConfig,
   entry: {
      'checkout-blocks-credit-card': path.resolve(
         process.cwd(),
         'src',
         'js',
         'checkout-blocks-credit-card',
         'index.js'
      ),
      'checkout-blocks-ideal': path.resolve(
         process.cwd(),
         'src',
         'js',
         'checkout-blocks-ideal',
         'index.js'
      ),
      'checkout-blocks-molpay-ml': path.resolve(
         process.cwd(),
         'src',
         'js',
         'checkout-blocks-molpay-ml',
         'index.js'
      ),
      'checkout-blocks-molpay-th': path.resolve(
         process.cwd(),
         'src',
         'js',
         'checkout-blocks-molpay-th',
         'index.js'
      ),
      'checkout-blocks-online-banking-poland': path.resolve(
         process.cwd(),
         'src',
         'js',
         'checkout-blocks-online-banking-poland',
         'index.js'
      ),
      'checkout-blocks-googlepay': path.resolve(
         process.cwd(),
         'src',
         'js',
         'checkout-blocks-googlepay',
         'index.js'
      ),
      'checkout-blocks-applepay': path.resolve(
         process.cwd(),
         'src',
         'js',
         'checkout-blocks-applepay',
         'index.js'
      ),
      'checkout-blocks-swish': path.resolve(
         process.cwd(),
         'src',
         'js',
         'checkout-blocks-swish',
         'index.js'
      ),
      'checkout-blocks-giropay': path.resolve(
         process.cwd(),
         'src',
         'js',
         'checkout-blocks-giropay',
         'index.js'
      ),
      'checkout-blocks-paypal': path.resolve(
         process.cwd(),
         'src',
         'js',
         'checkout-blocks-paypal',
         'index.js'
      ),
      'checkout-blocks-vipps': path.resolve(
         process.cwd(),
         'src',
         'js',
         'checkout-blocks-vipps',
         'index.js'
      ),
      'checkout-blocks-wechatpay': path.resolve(
         process.cwd(),
         'src',
         'js',
         'checkout-blocks-wechatpay',
         'index.js'
      ),
      'checkout-blocks-sofort': path.resolve(
         process.cwd(),
         'src',
         'js',
         'checkout-blocks-sofort',
         'index.js'
      ),
      'checkout-blocks-klarna': path.resolve(
         process.cwd(),
         'src',
         'js',
         'checkout-blocks-klarna',
         'index.js'
      ),
      'checkout-blocks-klarna-paynow': path.resolve(
         process.cwd(),
         'src',
         'js',
         'checkout-blocks-klarna-paynow',
         'index.js'
      ),
      'checkout-blocks-klarna-account': path.resolve(
         process.cwd(),
         'src',
         'js',
         'checkout-blocks-klarna-account',
         'index.js'
      ),
      'checkout-blocks-alipay': path.resolve(
         process.cwd(),
         'src',
         'js',
         'checkout-blocks-alipay',
         'index.js'
      ),
      'checkout-blocks-bancontact': path.resolve(
         process.cwd(),
         'src',
         'js',
         'checkout-blocks-bancontact',
         'index.js'
      ),
      'checkout-blocks-bancontact-mobile': path.resolve(
         process.cwd(),
         'src',
         'js',
         'checkout-blocks-bancontact-mobile',
         'index.js'
      ),
      'checkout-blocks-blik': path.resolve(
         process.cwd(),
         'src',
         'js',
         'checkout-blocks-blik',
         'index.js'
      ),
      'checkout-blocks-boleto': path.resolve(
         process.cwd(),
         'src',
         'js',
         'checkout-blocks-boleto',
         'index.js'
      ),
      'checkout-blocks-grabpay-sg': path.resolve(
         process.cwd(),
         'src',
         'js',
         'checkout-blocks-grabpay-sg',
         'index.js'
      ),
      'checkout-blocks-grabpay-my': path.resolve(
         process.cwd(),
         'src',
         'js',
         'checkout-blocks-grabpay-my',
         'index.js'
      ),
      'checkout-blocks-grabpay-ph': path.resolve(
         process.cwd(),
         'src',
         'js',
         'checkout-blocks-grabpay-ph',
         'index.js'
      ),
      'checkout-blocks-mobilepay': path.resolve(
         process.cwd(),
         'src',
         'js',
         'checkout-blocks-mobilepay',
         'index.js'
      ),
      'checkout-blocks-trustly': path.resolve(
         process.cwd(),
         'src',
         'js',
         'checkout-blocks-trustly',
         'index.js'
      ),
   },
   module: {
      ...defaultConfig.module,
      rules: [
         ...defaultRules,
         {
            test: /\.(sc|sa)ss$/,
            exclude: /node_modules/,
            use: [
               MiniCssExtractPlugin.loader,
               { loader: 'css-loader', options: { importLoaders: 1 } },
               {
                  loader: 'sass-loader',
                  options: {
                     sassOptions: {
                        includePaths: [ 'src/css' ],
                     },
                  },
               },
            ],
         },
      ],
   },
   plugins: [
      ...defaultConfig.plugins.filter(
         ( plugin ) =>
            plugin.constructor.name !== 'DependencyExtractionWebpackPlugin'
      ),
      new WooCommerceDependencyExtractionWebpackPlugin(),
      new MiniCssExtractPlugin( {
         filename: `[name].css`,
      } ),
   ],
};
