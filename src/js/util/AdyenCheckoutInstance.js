
export default async function AdyenCheckoutInstance (setEncryptedCardData) {

   if (typeof AdyenCheckout !== "function") {
      return new Promise(()=>{});
   }

   const woosa = adn_util;

   return await AdyenCheckout({
      paymentMethodsResponse: woosa.api.response_payment_methods,
      clientKey: woosa.api.origin_key,
      locale: woosa.locale,
      environment: woosa.api.environment,
      onChange: function (state, component) {

         if(state.data.paymentMethod.type == 'scheme'){

            if(state && state.isValid){

               var store_card = state.data.storePaymentMethod ? state.data.storePaymentMethod : '0';

               setEncryptedCardData({
                  encryptedCardNumber: state.data.paymentMethod.encryptedCardNumber,
                  encryptedExpiryMonth: state.data.paymentMethod.encryptedExpiryMonth,
                  encryptedExpiryYear: state.data.paymentMethod.encryptedExpiryYear,
                  encryptedSecurityCode: state.data.paymentMethod.encryptedSecurityCode,
                  holderName: state.data.paymentMethod.holderName,
                  storedPaymentMethodId: state.data.paymentMethod.storedPaymentMethodId,
                  store_card: store_card,
                  installments: !!state.data.installments ? state.data.installments.value : ''
               });

            } else {

               setEncryptedCardData({
                  encryptedCardNumber: '',
                  encryptedExpiryMonth: '',
                  encryptedExpiryYear: '',
                  encryptedSecurityCode: '',
                  holderName: '',
                  storedPaymentMethodId: '',
                  store_card: '',
                  installments: ''
               });

            }

         }else if(jQuery.inArray(state.data.paymentMethod.type, ['ideal', 'dotpay', 'molpay_ebanking_fpx_MY', 'molpay_ebanking_TH'] ) != -1){

            var value = '';

            if(state && state.isValid){
               value = state.data.paymentMethod.issuer;
            }

            setEncryptedCardData({
               issuer: value,
            });

         }else if(state.data.paymentMethod.type == 'blik'){

            var value = '';

            if(state && state.isValid){
               value = state.data.paymentMethod.blikCode;
            }

            setEncryptedCardData({
               code: value,
            });

         }

      },
      onAdditionalDetails: function (state, component) {

         let elem = jQuery(component._node),
            order_id = elem.attr('data-order_id');

         jQuery('.adn-component__text').show();

         jQuery.ajax({
            url   : woosa.ajax.url,
            method: 'POST',
            data: {
               action     : 'adn_additional_details',
               security   : woosa.ajax.nonce,
               state_data: state.data,
               order_id   : order_id
            },
            success: function(res) {
               if(res.data.redirect){
                  window.location.href = res.data.redirect;
               }
            }
         });

      },
      onError: function(err){
         console.log(err)
      }
   });

}