import PaymentAction from "../components/PaymentAction";
import CardForm from './components/CardForm';
import Label from "../components/Label";

const settings = window.wc.wcSettings.getSetting( 'woosa_adyen_credit_card_data', {
   icon: false,
   payment_action: false
} );
const title = window.wp.htmlEntities.decodeEntities( settings.title ) || window.wp.i18n.__( 'Adyen - Credit Card', 'integration-adyen-woocommerce' );

const Content = (props) => {

   const { eventRegistration, emitResponse } = props;

   return <>
      <CardForm
         installments={ settings.get_installments }
         stored_cards={settings.stored_cards}
         eventRegistration={eventRegistration}
         emitResponse={emitResponse}
      />
      {(!!settings.payment_action && Object.keys(settings.payment_action).length > 0) &&
         <PaymentAction
            action={settings.payment_action}
         />
      }
   </>;
};


const Adyen_Credit_Card_Block_Gateway = {
   name: 'woosa_adyen_credit_card',
   label: <Label text={title} image={settings.icon} name={'woosa_adyen_credit_card'} />,
   content: Object( window.wp.element.createElement )( Content, null ),
   edit: Object( window.wp.element.createElement )( Content, null ),
   canMakePayment: () => true,
   ariaLabel: title,
   supports: {
      features: settings.supports,
   },
};
window.wc.wcBlocksRegistry.registerPaymentMethod( Adyen_Credit_Card_Block_Gateway );