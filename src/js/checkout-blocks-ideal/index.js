import PaymentAction from "../components/PaymentAction";
import CardForm from '../components/CardForm';
import Label from "../components/Label";

const settings = window.wc.wcSettings.getSetting( 'woosa_adyen_ideal_data', {
   icon: false,
   payment_action: false
} );
const title = window.wp.htmlEntities.decodeEntities( settings.title ) || window.wp.i18n.__( 'Adyen - iDEAL', 'integration-adyen-woocommerce' );

const Content = (props) => {

   const { eventRegistration, emitResponse } = props;

   return <>
      <CardForm
         paymentMethod={'ideal'}
         eventRegistration={eventRegistration}
         emitResponse={emitResponse}
      />
      {(!!settings.payment_action && Object.keys(settings.payment_action).length > 0) &&
         <PaymentAction
            action={settings.payment_action}
         />
      }
   </>;
};


const Adyen_Ideal_Block_Gateway = {
   name: 'woosa_adyen_ideal',
   label: <Label text={title} image={settings.icon} name={'woosa_adyen_ideal'} />,
   content: Object( window.wp.element.createElement )( Content, null ),
   edit: Object( window.wp.element.createElement )( Content, null ),
   canMakePayment: () => true,
   ariaLabel: title,
   supports: {
      features: settings.supports,
   },
};
window.wc.wcBlocksRegistry.registerPaymentMethod( Adyen_Ideal_Block_Gateway );