//import React from 'react';

import Label from "../components/Label";

const settings = window.wc.wcSettings.getSetting( 'woosa_adyen_klarna_paynow_data', {
   icon: false
} );
const title = window.wp.htmlEntities.decodeEntities( settings.title ) || window.wp.i18n.__( 'Adyen - Klarna - Pay now', 'integration-adyen-woocommerce' );

const Content = () => {
   return window.wp.htmlEntities.decodeEntities( settings.description || '' );
};

const Adyen_Klarna_PayNow_Block_Gateway = {
   name: 'woosa_adyen_klarna_paynow',
   label:  <Label text={title} image={settings.icon} name={'woosa_adyen_klarna_paynow'} />,
   content: Object( window.wp.element.createElement )( Content, null ),
   edit: Object( window.wp.element.createElement )( Content, null ),
   canMakePayment: () => true,
   ariaLabel: title,
   supports: {
      features: settings.supports,
   },
};
window.wc.wcBlocksRegistry.registerPaymentMethod( Adyen_Klarna_PayNow_Block_Gateway );