import { useEffect, useState } from 'react';

import AdyenCheckoutInstance from "../util/AdyenCheckoutInstance";

export default function CardForm({ paymentMethod, eventRegistration, emitResponse }) {

   const [encryptedCardData, setEncryptedCardData] = useState({
      issuer: '',
   });

   const [isWasInit, setIsWasInit] = useState(false);


   const { onPaymentProcessing } = eventRegistration;


   function initAdyen() {

      AdyenCheckoutInstance(setEncryptedCardData).then(function(response){

         response.create(paymentMethod).mount('#'+'adn-'+paymentMethod+'-container');

         setIsWasInit(true);

      });

   }

   useEffect( () => {
      const unsubscribe = onPaymentProcessing( async () => {
         const customDataIsValid = !! encryptedCardData;

         if ( customDataIsValid ) {

            return {
               type: emitResponse.responseTypes.SUCCESS,
               meta: {
                  paymentMethodData: {
                     ["adn_"+paymentMethod+"_issuer"]: encryptedCardData.issuer,
                  },
               },
            };
         }

         return {
            type: emitResponse.responseTypes.ERROR,
            message: 'There was an error',
         };
      } );


      if (!isWasInit) {
         initAdyen();
      }

      return () => {
         unsubscribe();
      };
   }, [
      emitResponse.responseTypes.ERROR,
      emitResponse.responseTypes.SUCCESS,
      onPaymentProcessing,
      encryptedCardData
   ] );

   return (

      <div id={'adn-'+paymentMethod+'-container'}></div>

   );
}