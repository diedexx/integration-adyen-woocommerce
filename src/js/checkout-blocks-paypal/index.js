//import React from 'react';

import Label from "../components/Label";

const settings = window.wc.wcSettings.getSetting( 'woosa_adyen_paypal_data', {
   icon: false
} );
const title = window.wp.htmlEntities.decodeEntities( settings.title ) || window.wp.i18n.__( 'Adyen - Paypal', 'integration-adyen-woocommerce' );

const Content = () => {
   return window.wp.htmlEntities.decodeEntities( settings.description || '' );
};


const Adyen_Paypal_Block_Gateway = {
   name: 'woosa_adyen_paypal',
   label: <Label text={title} image={settings.icon} name={'woosa_adyen_paypal'} />,
   content: Object( window.wp.element.createElement )( Content, null ),
   edit: Object( window.wp.element.createElement )( Content, null ),
   canMakePayment: () => true,
   ariaLabel: title,
   supports: {
      features: settings.supports,
   },
};
window.wc.wcBlocksRegistry.registerPaymentMethod( Adyen_Paypal_Block_Gateway );