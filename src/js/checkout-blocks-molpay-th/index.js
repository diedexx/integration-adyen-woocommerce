import Label from "../components/Label";
import CardForm from '../components/CardForm';
import PaymentAction from "../components/PaymentAction";

const settings = window.wc.wcSettings.getSetting( 'woosa_adyen_molpay_th_data', {
   icon: false,
   payment_action: false
} );
const title = window.wp.htmlEntities.decodeEntities( settings.title ) || window.wp.i18n.__( 'Adyen - MOLPay - Thailand', 'integration-adyen-woocommerce' );

const Content = (props) => {

   const { eventRegistration, emitResponse } = props;

   return <>
      <CardForm
         paymentMethod={'molpay_ebanking_TH'}
         eventRegistration={eventRegistration}
         emitResponse={emitResponse}
      />
      {(!!settings.payment_action && Object.keys(settings.payment_action).length > 0) &&
         <PaymentAction
            action={settings.payment_action}
         />
      }
   </>;
};


const Adyen_MolpayTH_Block_Gateway = {
   name: 'woosa_adyen_molpay_th',
   label: <Label text={title} image={settings.icon} name={'woosa_adyen_molpay_th'} />,
   content: Object( window.wp.element.createElement )( Content, null ),
   edit: Object( window.wp.element.createElement )( Content, null ),
   canMakePayment: () => true,
   ariaLabel: title,
   supports: {
      features: settings.supports,
   },
};
window.wc.wcBlocksRegistry.registerPaymentMethod( Adyen_MolpayTH_Block_Gateway );