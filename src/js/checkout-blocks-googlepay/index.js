import PaymentAction from "../components/PaymentAction";
import GooglePay from './GooglePay';
import Label from "../components/Label";

const settings = window.wc.wcSettings.getSetting( 'woosa_adyen_googlepay_data', {
   icon: false,
   payment_action: false
} );
const title = window.wp.htmlEntities.decodeEntities( settings.title ) || window.wp.i18n.__( 'Adyen - Google Pay', 'integration-adyen-woocommerce' );

const Content = (props) => {

   const { eventRegistration, emitResponse } = props;

   return <>
      <GooglePay
         eventRegistration={eventRegistration}
         emitResponse={emitResponse}
         merchantIdentifier={settings.merchant_identifier}
         testmode={settings.testmode}
         description={settings.google_description}
         token={settings.token}
      />
      {(!!settings.payment_action && Object.keys(settings.payment_action).length > 0) &&
         <PaymentAction
            action={settings.payment_action}
         />
      }
   </>;
};


const Adyen_Googlepay_Block_Gateway = {
   name: 'woosa_adyen_googlepay',
   label: <Label text={title} image={settings.icon} name={'woosa_adyen_googlepay'} />,
   content: Object( window.wp.element.createElement )( Content, null ),
   edit: Object( window.wp.element.createElement )( Content, null ),
   canMakePayment: () => true,
   ariaLabel: title,
   supports: {
      features: settings.supports,
   },
};
window.wc.wcBlocksRegistry.registerPaymentMethod( Adyen_Googlepay_Block_Gateway );