import { useEffect, useState } from 'react';

export default function CardForm({ paymentMethod, eventRegistration, emitResponse }) {

   const [socialNumber, setSocialNumber] = useState('');

   const { onPaymentProcessing } = eventRegistration;

   useEffect( () => {
      const unsubscribe = onPaymentProcessing( async () => {
         if ( !!socialNumber ) {

            return {
               type: emitResponse.responseTypes.SUCCESS,
               meta: {
                  paymentMethodData: {
                     'woosa_adyen_boleto_social_number': socialNumber,
                  },
               },
            };
         }

         return {
            type: emitResponse.responseTypes.ERROR,
            message: window.wp.i18n.__( 'CPF/CNPJ number are empty, please input it and try again', 'integration-adyen-woocommerce' ),
         };
      } );

      return () => {
         unsubscribe();
      };
   }, [
      emitResponse.responseTypes.ERROR,
      emitResponse.responseTypes.SUCCESS,
      onPaymentProcessing,
      socialNumber
   ] );

   function handleSocialNumberChange(e) {
      setSocialNumber(e.target.value);
   }

   return (

      <div className="adyen-checkout__field adyen-checkout__card__holderName">
         <label className="adyen-checkout__label">
               <span className="adyen-checkout__label__text">
                  {window.wp.i18n.__( 'CPF/CNPJ number', 'integration-adyen-woocommerce' )} <abbr className="required" title="required">*</abbr>
               </span>
            <div className="adyen-checkout__input-wrapper">
               <input
                  className="adyen-checkout__input adyen-checkout__input--text"
                  type="text"
                  value={socialNumber}
                  onChange={handleSocialNumberChange}
               />
            </div>
         </label>
      </div>

   );
}