import Label from "../components/Label";
import CardForm from '../components/CardForm';
import PaymentAction from "../components/PaymentAction";

const settings = window.wc.wcSettings.getSetting( 'woosa_adyen_online_banking_poland_data', {
   icon: false,
   payment_action: false
} );
const title = window.wp.htmlEntities.decodeEntities( settings.title ) || window.wp.i18n.__( 'Adyen - Online Banking PL', 'integration-adyen-woocommerce' );

const Content = (props) => {

   const { eventRegistration, emitResponse } = props;

   return <>
      <CardForm
         paymentMethod={'onlineBanking_PL'}
         eventRegistration={eventRegistration}
         emitResponse={emitResponse}
      />
      {(!!settings.payment_action && Object.keys(settings.payment_action).length > 0) &&
         <PaymentAction
            action={settings.payment_action}
         />
      }
   </>;
};


const Adyen_Online_Banking_Poland_Block_Gateway = {
   name: 'woosa_adyen_online_banking_poland',
   label: <Label text={title} image={settings.icon} name={'woosa_adyen_online_banking_poland'} />,
   content: Object( window.wp.element.createElement )( Content, null ),
   edit: Object( window.wp.element.createElement )( Content, null ),
   canMakePayment: () => true,
   ariaLabel: title,
   supports: {
      features: settings.supports,
   },
};
window.wc.wcBlocksRegistry.registerPaymentMethod( Adyen_Online_Banking_Poland_Block_Gateway );