//import React from 'react';

const settings = window.wc.wcSettings.getSetting( 'woosa_adyen_blik_data', {
   icon: false,
   payment_action: false
} );

const title = window.wp.htmlEntities.decodeEntities( settings.title ) || window.wp.i18n.__( 'Adyen - BLIK', 'integration-adyen-woocommerce' );

import Label from "../components/Label";
import CardForm from './CardForm';
import PaymentAction from "../components/PaymentAction";

const Content = (props) => {

   const { eventRegistration, emitResponse } = props;

   return <>
      <CardForm
         paymentMethod={'blik'}
         eventRegistration={eventRegistration}
         emitResponse={emitResponse}
      />
      {(!!settings.payment_action && Object.keys(settings.payment_action).length > 0) &&
         <PaymentAction
            action={settings.payment_action}
         />
      }
   </>;
};


const Adyen_Blik_Block_Gateway = {
   name: 'woosa_adyen_blik',
   label: <Label text={title} image={settings.icon} name={'woosa_adyen_blik'} />,
   content: Object( window.wp.element.createElement )( Content, null ),
   edit: Object( window.wp.element.createElement )( Content, null ),
   canMakePayment: () => true,
   ariaLabel: title,
   supports: {
      features: settings.supports,
   },
};
window.wc.wcBlocksRegistry.registerPaymentMethod( Adyen_Blik_Block_Gateway );